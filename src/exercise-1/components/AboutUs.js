import React,{Component} from 'react';

class AboutUs extends Component{
  render(){
    return (
      <div>
        <p>Company: ThoughtWorks Local</p>
        <p>Location: Xi'an</p>
        <p>For more information please view our <a href='/'>website</a></p>
      </div>
    )
  };
}

export default AboutUs;