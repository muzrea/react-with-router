import React, {Component} from 'react';
import '../styles/App.css';
import {BrowserRouter as Router, Route, Link, NavLink, Switch} from 'react-router-dom';
import Home from "../../exercise-1/components/Home";
import Profile from "../../exercise-1/components/Profile";
import AboutUs from "../../exercise-1/components/AboutUs";
import Product from "../../exercise-2/component/Product";


class App extends Component {
  render() {
    return (
      <div className="app">
        <Router>
          <nav className='nav'>
            <div className='nav-item'>
              <NavLink exact to='/' className='nav-link' activeClassName='active-nav'>Home</NavLink>
              <NavLink exact to='/products' className='nav-link' activeClassName='active-nav'>Products</NavLink>
              <NavLink exact to='/my-profile' className='nav-link' activeClassName='active-nav'>My Profile</NavLink>
              <NavLink exact to='/about-us' className='nav-link' activeClassName='active-nav'>About Us</NavLink>
            </div>
          </nav>
          <Switch>
            <Route path='/' exact component={Home}/>
            <Route path='/my-profile' exact component={Profile}/>
            <Route path='/about-us' exact component={AboutUs}/>
            <Route path='/products' exact component={Product}/>
            <Route path='/goods' exact component={Product}/>
            <Route component={Home}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
