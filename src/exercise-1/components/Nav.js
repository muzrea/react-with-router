import React, {Component} from 'react';
import {BrowserRouter as Router, Link} from 'react-router-dom';

class Nav extends Component {
  render() {
    return (
      <Router>
        <nav>
          <Link to='/'>Home</Link>
          <Link to='/my-profile'>My Profile</Link>
          <Link to='/about-us'>About Us</Link>
        </nav>
      </Router>
    )
  };
}

export default Nav;
