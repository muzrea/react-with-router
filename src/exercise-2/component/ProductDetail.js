import React, {Component} from 'react';

class ProductDetail extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log(this.props.product);
    return (
      <div>
        <p> Product Details:</p>
        <p>Name: {this.props.product.name}</p>
        <p>Id: {this.props.product.id}</p>
        <p>Price: {this.props.product.price}</p>
        <p>Quantity: {this.props.product.quantity}</p>
        <p>Desc: {this.props.product.desc}</p>
        <p>URL: /products/{this.props.product.id}</p>
      </div>
    )
  };
}

export default ProductDetail;