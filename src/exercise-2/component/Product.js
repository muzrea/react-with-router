import React, {Component} from 'react';
import {BrowserRouter as Router, NavLink, Switch, Route} from 'react-router-dom';
import data from '../mockups/data';

class Product extends Component {

  render() {
    const ProductDetail = ({match}) => {
      let productId = match.params.id;
      let productDetail = Object.values(data)[productId - 1];
      return (
        <div>
          <h2> Product Details:</h2>
          <p>Name: {productDetail.name}</p>
          <p>Id: {productDetail.id}</p>
          <p>Price: {productDetail.price}</p>
          <p>Quantity: {productDetail.quantity}</p>
          <p>Desc: {productDetail.desc}</p>
          <p>URL: {match.url}</p>
        </div>
      )
    };
    return (
      <div>
        <p>All Products:</p>
        <Router>
          <ul>
            <li>
              <NavLink exact to='/products/1'>Bicycle</NavLink>
            </li>
            <li>
              <NavLink exact to='/products/2'>TV</NavLink>
            </li>
            <li>
              <NavLink exact to='/products/3'>Pencil</NavLink>
            </li>
          </ul>
          <Switch>
            <Route path='/products/:id' component={ProductDetail}/>
          </Switch>
        </Router>
      </div>
    )
  };
}

export default Product;